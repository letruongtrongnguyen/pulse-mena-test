import 'package:get_it/get_it.dart';
import 'package:pulse_mena/providers/feed_screen_provider/feed_screen_provider.dart';

final getIt = GetIt.instance;

Future<void> configureInjection() async {
  //Providers
  getIt.registerSingleton<FeedScreenProvider>(FeedScreenProvider());
}
