import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pulse_mena/configs/enviroment.dart';
import 'package:pulse_mena/core/assets_path.dart';
import 'package:pulse_mena/core/router/router.gr.dart';
import 'package:pulse_mena/core/size_config/size_config.dart';
import 'package:pulse_mena/widgets/basic/country_widget.dart';
import 'package:pulse_mena/widgets/basic/mena_text_widget.dart';

class TestScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    print(Environment.instance.appEnvironment);

    return Scaffold(
      appBar: AppBar(
        title: Text('appbarTitle'),
      ),
//      backgroundColor: Colors.black,
      body: Container(
        child: Center(
          child: Column(
            children: [
              MenaText("Hi."),
              FlatButton(
                onPressed: () {
//                  I18n.of(context).locale = Locale('vi', '');
//                  context.rootNavigator.push(Routes.feedScreen);
                  ExtendedNavigator.of(context).pushAndRemoveUntilPath(
                      Routes.testScreen, null);
                },
                child: MenaText("Click me"),
              ),
              MenaCountry(
                code: "vn",
                showCountryName: false,
              ),
              ShaderMask(
                shaderCallback: (bounds) {
                  return LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color(0xffFF7817), Color(0xffD600C0)],
                  ).createShader(bounds);
                },
                child: SvgPicture.asset(
                  AssetsPath.fromImagesSvg('heart_ic'),
                  semanticsLabel: 'A red up arrow',
                  height: 100,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
