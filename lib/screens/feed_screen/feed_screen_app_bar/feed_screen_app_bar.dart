import 'package:cached_network_image/cached_network_image.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pulse_mena/core/size_config/size_config.dart';
import 'package:pulse_mena/providers/feed_screen_provider/feed_screen_provider.dart';
import 'package:pulse_mena/widgets/basic/mena_text_widget.dart';

import '../../../dependency_injection.dart';

class FeedScreenAppBar extends StatefulWidget implements PreferredSizeWidget {
  final TabController tabController;

  const FeedScreenAppBar({
    Key key,
    this.tabController,
  }) : super(key: key);

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(60);

  _FeedScreenAppBarState createState() => _FeedScreenAppBarState();
}

class _FeedScreenAppBarState extends State<FeedScreenAppBar> {
  @override
  void initState() {
    // TODO: implement initState
    widget.tabController.animation
      ..addListener(() {
        getIt<FeedScreenProvider>().onActiveTabChange(
            FeedScreenTabType.values[widget.tabController.index]);
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
      ),
      child: Container(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.black,
              Colors.black.withOpacity(0.5),
              Colors.black.withOpacity(0),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        alignment: Alignment.centerLeft,
        child: Container(
          height: widget.preferredSize.height,
          color: Colors.transparent,
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              AnimatedPositioned(
                duration: Duration(milliseconds: 400),
                child: _buildUserAvatar(context),
              ),
              _buildTabBar(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildUserAvatar(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 10),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 0,
            blurRadius: 15,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(40.0),
          bottomRight: Radius.circular(40.0),
        ),
      ),
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(45),
          child: CachedNetworkImage(
            imageUrl:
                "https://s3-alpha-sig.figma.com/img/42c7/e615/3794f6aeaab372c982c6a3d30d851f8b?Expires=1601251200&Signature=hv9r2fNJn17of1ex6gnEsfVVNZwAJfic8IbwaWeoqYTx91Rqo0MDPsALvxmlD9KPf6Wxmuo~m-NeLiZr2MhAlAQL1HG0~AEJMwDk15St9ee93uea-c7CaqlMaOb3IL8-1n1zYjZRg1ar6B7VwPOacgxri-Mv4Bz0CbQHrOR68ukDG2v5b9GyP~oKflrf1u8hnvsJtppADacWiG5aVpdcYc03pNkIDxF4gQOLY~Z3iWmiNf8UzYANF0MjsSqP0DOQocbadX5V00HABChGv1EMQFIESwVVIPlVBA2vVBuYDzlkJVJXS5M7Qqb7wFKqHNidATB9s52j244ejwLIzmln-w__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
          ),
        ),
      ),
    );
  }

  Widget _buildTabBar(BuildContext context) {
    final activeTab = context.select<FeedScreenProvider, FeedScreenTabType>(
      (p) => p.currentActiveTab,
    );

    return Container(
      width: SizeConfig.screenWidth,
      alignment: Alignment.center,
      child: TabBar(
        indicator: _TabIndicator(
          color: Colors.white,
          width: 40,
          height: 2,
        ),
        isScrollable: true,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 7,
        ),
        controller: widget.tabController,
        tabs: [
          ...List.generate(
            3,
            (index) => _tabBarItem(
              context,
              text:
                  "feed.tabTitle.${EnumToString.convertToString(FeedScreenTabType.values[index])}",
              active: activeTab == FeedScreenTabType.values[index],
            ),
          ),
        ],
      ),
    );
  }

  Widget _tabBarItem(
    BuildContext context, {
    bool active = false,
    String text,
  }) {
    final titleColor = active ? Colors.white : Colors.white.withOpacity(0.4);

    return AnimatedContainer(
      duration: Duration(milliseconds: 10),
//      color: Colors.black,
      child: MenaText(
        text,
        style: TextStyle(
          fontSize: 11,
          color: titleColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class _TabIndicator extends Decoration {
  final BoxPainter _painter;

  _TabIndicator({
    @required Color color,
    @required double width,
    @required double height,
  }) : _painter = _TabIndicatorPainter(color, width, height);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _TabIndicatorPainter extends BoxPainter {
  final Paint _paint;
  final double width;
  final double height;

  _TabIndicatorPainter(Color color, this.width, this.height)
      : _paint = Paint()
          ..color = color
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset = offset +
        Offset(cfg.size.width / 2 - (width / 2), cfg.size.height - height);
    canvas.drawRect(circleOffset & Size(width, height), _paint);
  }
}
