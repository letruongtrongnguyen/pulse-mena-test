import 'package:flutter/material.dart';
import 'package:pulse_mena/providers/feed_screen_provider/feed_screen_provider.dart';
import 'package:pulse_mena/screens/feed_screen/feed_screen_app_bar/feed_screen_app_bar.dart';

import '../../core/size_config/size_config.dart';
import 'feed_screen_body/feed_screen_body.dart';

class FeedScreen extends StatefulWidget {
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    _tabController =
        new TabController(vsync: this, length: FeedScreenTabType.values.length);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      appBar: FeedScreenAppBar(tabController: _tabController),
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      body: FeedScreenBody(tabController: _tabController),
    );
  }
}
