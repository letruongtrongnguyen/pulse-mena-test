import 'package:flutter/material.dart';

class MyHostScreen extends StatefulWidget {
  @override
  _MyHostScreenState createState() => _MyHostScreenState();
}

class _MyHostScreenState extends State<MyHostScreen> {
  PageController _pageController;
  double offsetA = 0.0;

  @override
  void initState() {
    _pageController = PageController(
      keepPage: true,
    );

    _pageController.addListener(() {
      setState(() {
        offsetA = _pageController.offset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView(
        controller: _pageController,
        scrollDirection: Axis.vertical,
        children: [
          ...List.generate(
            100,
            (counter) => Container(),
          )
        ],
      ),
    );
  }
}
