import 'package:flutter/material.dart';

import 'tab_screens/myHost_screen.dart';
import 'tab_screens/newPost_screen.dart';
import 'tab_screens/popular_screen.dart';

class FeedScreenBody extends StatelessWidget {
  final TabController tabController;

  FeedScreenBody({
    this.tabController,
  });

  @override
  Widget build(BuildContext context) {
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (OverscrollIndicatorNotification overScroll) {
        overScroll.disallowGlow();
        return;
      },
      child: TabBarView(
        controller: tabController,
        physics: ScrollPhysics(),
        children: [
          MyHostScreen(),
          PopularScreen(),
          NewFeedScreen(),
        ],
      ),
    );
  }
}
