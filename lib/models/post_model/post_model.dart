import 'package:flutter/foundation.dart';

class PostModel extends ChangeNotifier {
  final String id;
  final String postText;
  final String videoUrl;
  final String imageUrl;
  bool isLiked;
  int noOfLike;
  int noOfComment;

  PostModel({
    this.id,
    this.postText,
    this.videoUrl,
    this.imageUrl,
    this.isLiked = false,
    this.noOfComment = 0,
    this.noOfLike = 0,
  });

  void toggleLike() {
    if (isLiked) {
      isLiked = false;
      noOfLike--;
      notifyListeners();
      return;
    }

    isLiked = true;
    noOfLike++;
    notifyListeners();
  }
}
