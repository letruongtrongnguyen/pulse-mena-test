import 'package:pulse_mena/core/utils.dart';

class StringModule {
  static String capitalize(String str) {
    if (Utils.isEmpty(str)) return str;

    List<String> splited = str.split(" ");
    List<String> res = [];
    splited.forEach((o) {
      res.add("${o[0].toUpperCase()}${o.substring(1)}");
    });

    return res.join(" ");
  }
}
