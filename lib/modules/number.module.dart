import 'package:intl/intl.dart';

class NumberModule {
  static format(num value) {
    return new NumberFormat("#,###", "en_US").format(value);
  }

  static String display(num value){
    if(value < 10) return "0$value";

    return value.toInt().toString();
  }
}
