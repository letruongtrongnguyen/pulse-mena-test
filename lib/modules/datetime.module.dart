import 'number.module.dart';

class DateTimeModule {
  static getMondayUTC() {
    final today = DateTime.now().toUtc();
    return DateTime.utc(today.year, today.month, today.day - today.weekday + 1);
  }

  static int getCachedTimestampUTC() {
    final now = DateTime.now();
    final time = DateTime.parse(
            "${NumberModule.display(now.year)}-${NumberModule.display(now.month)}-${NumberModule.display(now.day)} ${NumberModule.display(now.hour)}:00:00")
        .toUtc();
    return (time.millisecondsSinceEpoch / 1000).round();
  }

  static DateTime parseFromTimestamp(int timestamp) {
    return new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  }
}
