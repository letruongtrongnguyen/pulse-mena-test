import 'package:flutter/foundation.dart';

enum AppEnvironment { DEV, STAGING, PROD }

class EnvValue {
  EnvValue({
    @required this.apiUrl,
  });

  final String apiUrl;
//Add other flavor specific values, e.g database name
}

class Environment {
  final AppEnvironment appEnvironment;

  final EnvValue envValue;
  static Environment _instance;

  factory Environment({
    @required AppEnvironment appEnvironment,
    @required EnvValue envValue,
  }) {
    _instance ??= Environment._internal(appEnvironment, envValue         );
    return _instance;
  }

  Environment._internal(this.appEnvironment, this.envValue);

  static Environment get instance {
    return _instance;
  }

  static bool isProduction() => _instance.appEnvironment == AppEnvironment.PROD;

  static bool isDevelopment() => _instance.appEnvironment == AppEnvironment.DEV;

  static bool isStaging() => _instance.appEnvironment == AppEnvironment.STAGING;
}

class EnvironmentConfig {
  static development() {
    Environment(
      appEnvironment: AppEnvironment.DEV,
      envValue: EnvValue(
        apiUrl: "https://stag-pulse-api-v2.mocogateway.com",
      ),
    );
  }

  static staging() {
    Environment(
      appEnvironment: AppEnvironment.STAGING,
      envValue: EnvValue(
        apiUrl: "https://stag-pulse-api-v2.mocogateway.com",
      ),
    );
  }

  static production() {
    Environment(
      appEnvironment: AppEnvironment.PROD,
      envValue: EnvValue(
        apiUrl: "https://pulse-api-v2.mocogateway.com",
      ),
    );
  }
}
