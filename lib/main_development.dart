import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app.dart';
import 'configs/enviroment.dart';
import 'configs/theme.dart';
import 'dependency_injection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(AppTheme.status.style);

  await configureInjection();

  EnvironmentConfig.development();

  runApp(MyApp());
}
