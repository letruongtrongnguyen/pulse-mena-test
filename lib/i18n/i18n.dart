import 'package:flutter/material.dart';
import 'package:pulse_mena/configs/general.dart';

export 'en.dart';
export 'vi.dart';

const List<Locale> supportedLocales = [
  const Locale('en', ''),
  const Locale('vi', ''),
];

final Locale defaultLanguage = Locale(AppSettings.defaultLang, '');
