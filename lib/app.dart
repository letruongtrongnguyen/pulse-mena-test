import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:pulse_mena/configs/general.dart';
import 'package:pulse_mena/i18n/i18n.dart';

import 'core/provider/provider_list.dart';
import 'core/router/router.gr.dart';

class MyApp extends StatefulWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ProviderList(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: AppSettings.APP_TITLE,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
        builder: ExtendedNavigator.builder(
          router: Router(),
//          initialRoute: Routes.testScreen,
          builder: (BuildContext context, Widget child) => I18n(
            initialLocale: defaultLanguage,
            child: child,
          ),
        ),
      ),
    );
  }
}
