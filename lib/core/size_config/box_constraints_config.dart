class BoxConstraintConfig {
  final double percentWidth;
  final double percentHeight;

  BoxConstraintConfig(this.percentWidth, this.percentHeight);
}
