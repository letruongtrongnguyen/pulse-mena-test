import 'dart:convert';
import 'dart:math';

class Utils {
  static String randomString(int length) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();

    return String.fromCharCodes(Iterable.generate(
        length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  static bool isEmpty(dynamic object) {
    if (object == null) return true;

    if (object is String) return object == "";

    if (object is List) return object.isEmpty;

    return false;
  }

  static String clearZeroWidthSpace(String str) {
    try {
      List<int> bytes = str.toString().codeUnits;
      return utf8.decode(bytes).replaceAll("", "\u{200B}");
    } catch (e) {
      print("Error when handle utf8 with string: $str");
      print(e);
      return str.replaceAll("", "\u{200B}");
    }
  }
}
