import 'package:flutter/foundation.dart';
import 'dart:async';

class DeBouncer {
  static const defaultDebounceDuration = Duration(milliseconds: 400);

  final Duration duration;
  VoidCallback action;
  Timer _timer;

  DeBouncer({this.duration = defaultDebounceDuration});

  run(VoidCallback action) {
    if (_timer != null) {
      _timer.cancel();
    }

    _timer = Timer(duration, action);
  }
}
