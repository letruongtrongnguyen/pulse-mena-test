import 'package:flutter/foundation.dart';

import '../../core/app_error.dart';

class LoadingErrorChangeNotifier extends ChangeNotifier {
  bool _loading = false;
  AppError _error;

  bool get loading => _loading;

  AppError get error => _error;

  void toggleLoading(bool newLoading) {
    _loading = newLoading == null ? !_loading : newLoading;
  }

  void setError(AppError newError) {
    newError.toString();
    _error = newError;
  }
}
