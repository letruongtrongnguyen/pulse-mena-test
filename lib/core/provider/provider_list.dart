import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../dependency_injection.dart';
import '../../providers/feed_screen_provider/feed_screen_provider.dart';
import '../../providers/test_provider.dart';

class ProviderList extends StatelessWidget {
  const ProviderList({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => getIt<TestProvider>()),
        ChangeNotifierProvider(create: (_) => getIt<FeedScreenProvider>()),
      ],
      child: child,
    );
  }
}
