class AppError {
  final String message;

  AppError(this.message);

  @override
  String toString() {
    if (message == null) return 'Unknown Error';
    return message;
  }
}
