class AssetsPath {
  static String fromImagesCommon(String name, [String extension]) =>
      'assets/images/common/$name.${extension != null ? extension : 'png'}';

  static String fromImagesFlags(String name, [String extension]) =>
      'assets/images/flags/$name.${extension != null ? extension : 'svg'}';

  static String fromImagesSvg(String name, [String extension]) =>
      'assets/images/svg/$name.${extension != null ? extension : 'svg'}';

  static String fromImagesGifs(String name, [String extension]) =>
      'assets/images/gifs/$name.${extension != null ? extension : 'gif'}';

  static String fromVideos(String name, [String extension]) =>
      'assets/videos/$name.${extension != null ? extension : 'mp4'}';
}
