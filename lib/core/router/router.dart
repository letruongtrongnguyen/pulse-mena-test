import 'package:auto_route/auto_route_annotations.dart';
import 'package:pulse_mena/screens/feed_screen/feed_screen.dart';
import 'package:pulse_mena/screens/test_screen.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    // initial route is named "/"
    MaterialRoute(page: FeedScreen, initial: true),
    MaterialRoute(page: TestScreen),
  ],
)
class $Router {}
