// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../screens/feed_screen/feed_screen.dart';
import '../../screens/test_screen.dart';

class Routes {
  static const String feedScreen = '/';
  static const String testScreen = '/test-screen';
  static const all = <String>{
    feedScreen,
    testScreen,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.feedScreen, page: FeedScreen),
    RouteDef(Routes.testScreen, page: TestScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    FeedScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => FeedScreen(),
        settings: data,
      );
    },
    TestScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => TestScreen(),
        settings: data,
      );
    },
  };
}
