import 'package:flutter/material.dart';
import "package:i18n_extension/i18n_extension.dart";
import 'package:pulse_mena/configs/theme.dart';
import 'package:pulse_mena/i18n/i18n.dart';
import 'package:pulse_mena/modules/string.module.dart';

class MenaText extends StatelessWidget {
  String text;
  final TextStyle style;
  final StrutStyle strutStyle;
  final TextAlign textAlign;
  final TextDirection textDirection;
  final Locale locale;
  final bool softWrap;
  final TextOverflow overflow;
  final double textScaleFactor;
  final int maxLines;
  final String semanticsLabel;
  final TextWidthBasis textWidthBasis;
  final TextHeightBehavior textHeightBehavior;
  final bool capitalize;
  final bool uppercase;

  MenaText(
    this.text, {
    Key key,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
    this.capitalize = false,
    this.uppercase = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    this.text = this.text.i18n;

    if (this.capitalize) this.text = StringModule.capitalize(this.text);
    if (this.uppercase) this.text = this.text.toUpperCase();

    TextStyle textStyle = TextStyle(
      decoration: TextDecoration.none,
      color: AppTheme.font.color,
      fontSize: AppTheme.font.size,
      fontFamily: AppTheme.font.family,
      fontWeight: FontWeight.w400,
    ).merge(this.style);

    return Text(
      this.text,
      key: this.key,
      style: textStyle.merge(new TextStyle(
        fontSize:
            textStyle.fontSize * (MediaQuery.of(context).size.width / 320),
      )),
      strutStyle: this.strutStyle,
      textAlign: this.textAlign,
      textDirection: this.textDirection,
      locale: this.locale,
      softWrap: this.softWrap,
      overflow: this.overflow,
      textScaleFactor: this.textScaleFactor,
      maxLines: this.maxLines,
      semanticsLabel: this.semanticsLabel,
      textWidthBasis: this.textWidthBasis,
      textHeightBehavior: this.textHeightBehavior,
    );
  }
}

extension Localization on String {
  String get i18n => localize(this, t);
  static var t = Translations.byLocale("en") + {"en": en} + {"vi": vi};
}
