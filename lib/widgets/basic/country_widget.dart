import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pulse_mena/core/size_config/size_config.dart';
import 'package:pulse_mena/modules/country.module.dart';

import 'mena_text_widget.dart';

class MenaCountry extends StatelessWidget {
  final String code;
  final TextStyle style;
  final int width;
  final bool showCountryName;

  const MenaCountry({
    Key key,
    this.code,
    this.style,
    this.width,
    this.showCountryName = true,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(
          CountryModule.getFlagSvgPath(code: this.code),
          width: width ?? SizeConfig.safeBlockHorizontal * 10,
        ),
        if (showCountryName) SizedBox(width: 5),
        if (showCountryName)
          MenaText(
            CountryModule.getName(code: this.code),
            style: TextStyle(
              fontSize: 10,
            ).merge(this.style),
          ),
      ],
    );
  }
}
