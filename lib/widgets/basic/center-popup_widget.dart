import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

import 'mena_text_widget.dart';

void showCenterPopup({
  BuildContext context,
  Color barrierColor,
  Color backgroundColor,
  double width,
  double height,
  bool isDismissible = true,
  BorderRadius borderRadius,
  Widget content,
  List<Map<String, dynamic>> actions,
}) {
  assert(context != null);

  showModal(
    context: context,
    configuration: FadeScaleTransitionConfiguration(
      barrierDismissible: isDismissible,
      barrierColor: barrierColor ?? Colors.black.withOpacity(0.8),
    ),
    builder: (ctx) => _CenterPopup(
      context,
      width: width ?? 100,
      height: height ?? 200,
      actions: actions,
      borderRadius: borderRadius,
      backgroundColor: backgroundColor,
      content: content,
    ),
  );
}

class _CenterPopup extends StatelessWidget {
  final BuildContext popupContext;
  final double width;
  final double height;
  final BorderRadius borderRadius;
  final Color backgroundColor;
  final Widget content;
  final List<Map<String, dynamic>> actions;

  _CenterPopup(
    this.popupContext, {
    this.width,
    this.height = 200,
    this.actions,
    this.borderRadius,
    this.content,
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    final sizeConfig = MediaQuery.of(popupContext);
    final Size screen = sizeConfig.size;

    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: (this.width != null && screen.width > this.width)
            ? (screen.width - this.width) / 2
            : 0,
        vertical: (screen.height > this.height)
            ? (screen.height -
                    sizeConfig.padding.top -
                    sizeConfig.padding.bottom -
                    this.height) /
                2
            : 0,
      ),
      child: ClipRRect(
        borderRadius: borderRadius ?? BorderRadius.circular(15),
        child: Material(
          color: backgroundColor ?? Colors.white,
          child: Column(
            children: [
              Container(
                height: actions != null ? this.height - 60 : this.height,
                width: this.width,
                child: content ?? SizedBox.shrink(),
              ),
              if (actions != null) Spacer(),
              if (actions != null)
                Row(
                  children: [
                    ...actions
                        .asMap()
                        .map(
                          (i, e) => MapEntry(
                            i,
                            GestureDetector(
                              onTap: () {
                                if (e["action"] != null) e["action"]();
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: this.width / actions.length,
                                height: 60,
                                child: Center(
                                  child: MenaText(
                                    e["title"],
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  border: Border(
                                    top: BorderSide(
                                      width: 1,
                                      color: Color(0xffebecec),
                                    ),
                                    right: BorderSide(
                                      width: i == actions.length - 1 ? 0 : 1,
                                      color: i == actions.length - 1
                                          ? Colors.transparent
                                          : Color(0xffebecec),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                        .values
                        .toList(),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
