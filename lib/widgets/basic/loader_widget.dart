import 'package:flutter/material.dart';

class MenaLoader extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff26d57d)),
        ),
      ),
    );
  }
}
