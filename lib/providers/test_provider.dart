import 'package:flutter/material.dart';

class TestProvider extends ChangeNotifier {
  // Stats: start
  String _currentActiveTab = "aaa";
  String _previousActiveTab = "aaa";

// Stats: end

// Getters: start
  String get currentActiveTab => _currentActiveTab;

  String get previousActiveTab => _previousActiveTab;

// Getters: end
}
