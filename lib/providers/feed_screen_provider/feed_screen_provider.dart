import 'package:flutter/cupertino.dart';

enum FeedScreenTabType {
  myHost,
  popular,
  newPost,
}

class FeedScreenProvider extends ChangeNotifier {
// Stats: start
  FeedScreenTabType _currentActiveTab = FeedScreenTabType.myHost;
  FeedScreenTabType _previousActiveTab = FeedScreenTabType.myHost;

// Stats: end

// Getters: start
  FeedScreenTabType get currentActiveTab => _currentActiveTab;

  FeedScreenTabType get previousActiveTab => _previousActiveTab;

// Getters: end

// Methods: start
  void onActiveTabChange(FeedScreenTabType newTab) {
    if (_currentActiveTab == newTab) return;
    _previousActiveTab = _currentActiveTab;
    _currentActiveTab = newTab;
    notifyListeners();
  }

// Methods: end
}
